<img src="./images/jinja_logo.png" height="120" />
<img src="./images/archlinux-logo.png" height="100" />

This guide is about installing ArchLinux and making a RICE customization.
</br>
</br>
</br>
# Table of contents
1. [Why ArchLinux](#why-archlinux)
2. [What is RICE](#what-is-rice)
3. [Step-by-Step installation]()
</br>
</br>
</br>

# Why ArchLinux?
- Is a good "fresh" operating system without having to build packages from source
- Is not a full OS with many thing you don't want.
- Arch allow you tu build you system with the tools you like, only.

# What is RICE
RICE stands for _**R**acing **I**nspired **C**osmetic **E**nhancements_. Other words, just a full _"visual"_  customization to look prettier and personal.
</br>
</br>
</br>

# Step-by-Step Installation
To begin, put a good relaxing song... Here is my recommendation [Fortnite Mushroom Lofi - Server Down](https://www.youtube.com/watch?v=IguJSV2GwC8)

The first step we need to do is the installation of ArchLinux itself, after that, we'll make some configurations on arch to guarantee he is stable, the fonts we will use, etc. And the final step is the tools we will install the packages needed to configure the Environment.

> Obs.: This steps is for a EFI system.

</br>
</br>

## Installing Arch Linux
First, [download arch linux](https://www.archlinux.org/download/) and create a LiveUSB

After booting, the fun begins...

### Set the keyboard layout
```bash
$ loadkeys br-abnt2
```
I'm from Brazil, so I will set the one from my country, in case you need to check what is for your country run 
```bash
$ ls /usr/share/kbd/keymaps/**/*.map.gz`
```

### Partitioning the disk
Then we will partition the disks, to do this, lets list to check the hard drives
```bash
$ fdisk -l
```

After finding what is the hard drive to use, commonly is the `sda`, so we are going to use that.
```bash
$ fdisk /dev/sda
```

In fdisk, lets create our partitions, first we need the ESP partition, in fdisk run this commands:
```
Command: n
Partition Type: p
Partition Number: 1
First Sector: (leave blank)
Last Sector: +512M

# Let's change the type also
Command: t
Hex code or alias: ef
```
So far, so good, now we need our root partition for our system and home. Still in fdisk run
```
Command: n
Partition Type: p
Partition Number: 2
First Sector: (Leave blank)
Last Sector: (Leave Blank to get all the hard drive remaining space)
```
In this case, there is no need to change the format type of the partition. Now, let's write our partition to the partition table, in fdisk run
```
Command: w
```
And Done, we have our partition.

Now, we need to crete the filesystems

```bash
# ESP filesystem creation
$ mkfs.fat -F32 /dev/sda1

# Root filesystem creation
$ mkfs.ext4 /dev/sda2
```

### Connect to wifi (if necessary)
Let's load iwctl tool to connect
```bash
$ iwctl
```

Now, on iwctl console, let's run the configuration
``` bash
# list wireless devices
device list

# After selecting the device (let's say it is wlan0)
# Scan the available networks
station wlan0 scan

# Now, connect to the wifi
station wlan0 connect "WIFI-NETWORK-NAME"
```

Outside the `iwctl` run the command to check for internet connection
```bash
$ ping archlinux.org
```

### Select mirror
Now, let's select our mirror to download packages from

First, install `reflector`, this is a package that finds the best mirrors for your location.

```bash
# Update package databases
$ pacman -Sy

$ pacman -S reflector

# Make a backup of current mirror list
$ cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak

# Run reflector to generate new list
$ reflector -c "BR" -f 12 -l 12 -n 10 --save /etc/pacman.d/mirrorlist
```
You must change the params:
- -c: is the country, I used BR for Brazil (my country)
- -f: n of the fastest servers
- -l: n servers most recently synchronized
- -n: total of servers to save on list

### Install arch linux
Now, the time we all were waiting, installing arch linux itself

Mount the partition
```bash
$ mount /dev/sda2 /mnt
```

Using `pacstrap`, install the base packages
```bash
$ pacstrap /mnt base linux linux-firmware vim
```
> Obs.: Here I use `vim`, because I like, but you can change it to `nano`

Now, generate the fstab file
```bash
$ genfstab -U /mnt >> /mnt/etc/fstab
```

And enter in the system
```bash
$ arch-chroot /mnt
```

Set the timezone
```bash
$ ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
$ hwclock --systohc
```
> Obs.: Again, remember to change to your location, swap `America/Sao_Paulo` to yours.

Set up Locale
```bash
vim /etc/locale.gen
```
uncomment the line with you language, I will use en_US
```
#en_US.UTF-8 UTF-8
```
And run
```bash
$ locale-gen
$ echo LANG=en_US.UTF-8 > /etc/locale.conf
$ export LANG=en_US.UTF-8
```

And pin a keymap to the device
```bash
$ vim /etc/vconsole.conf

# Add line
KEYMAP=br-abnt2
```
> Obs.: Remember to set your keymap, the same you used on loadkeys.

Configure the networking (change `desktop` to a name you like)
```bash
$ echo desktop > /etc/hostname
$ touch /etc/hosts
$ vim /etc/hosts
```
And add to the file
```
127.0.0.1       localhost
::1             localhost
127.0.1.1       desktop
```
Save and close the file

Set the root password
```bash
$ passwd
```

Install the GRUB Bootloader
```bash
$ pacman -S grub efibootmgr
```

Create the directory for EFI
```bash
$ mkdir /boot/efi
```

Mount the EFI
```bash
$ mount /dev/sda1 /boot/efi
```

Install  the bootloader
```bash
$ grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi
$ grub-mkconfig -o /boot/grub/grub.cfg
```

Install sudo
```bash
$ pacman -S sudo
```

Create the user
```bash
$ useradd -m USERNAME
$ passwd USERNAME # To set the password for the user
```
Add the new user to required groups
```bash
$ usermod -aG wheel,audio,video,storage USERNAME
```
And allow the group `wheel` to run sudo commands
```bash
$ visudo
```
And uncomment the line
```
%wheel ALL=(ALL:ALL) ALL
```

Finally, we need a network manager to connect after installation
```bash
$ pacman -S networkmanager
$ systemctl enable NetworkManager.service
```

And we are done with the first step, leave the chroot
```bash
$ exit
```
Umount the partition
```bash
$ umount -l /mnt
```
And shutdown
```bash
$ shutdown now
```

Base installation of arch is done, when you are reade, boot the machine and proceed with the rest of the installation!

## After install _"essential"_ setup
After the initial step of installation, let's start installing the basics to call this a working desktop.

Point to a LTS kernel
```bash
$ sudo pacman -S linux-lts
$ sudo pacman -S linux-lts-headers
$ sudo pacman -Rs linux
```
By now, we need to remake the grub configuration
```bash
$ sudo grub-mkconfig -o /boot/grub/grub.cfg
```

I strongly recommend a restart on the computer now.

Now, we add a minimal security with a firewall
```bash
$ sudo pacman -S ufw
```

Now we enable it to start on boot
```bash
$ sudo systemctl enable ufw ---now
$ sudo ufw enable
```

And last, let's deny all incoming and allor all outgoing connections
```bash
$ sudo ufw default deny incoming
$ sudo ufw deafult allow outgoing
```

To install packages from AUR, let's use YAY
```bash
$ sudo pacman -S --needed base-devel git

$ git clone https://aur.archlinux.org/yay.git
$ cd yay

$ makepkg -si
$ yay --version # Just check if everything is ok

$ cd ..
$ rm -rf yay
```

Some basic tools
```bash
$ sudo pacman -S wget tar unzip a52dec faac faad2 flac jasper lame libdca libdv libmad libmpeg2 libtheora libvorbis libxv wavpack x264 xvidcore
```

Now, let's install the DE tools

Starting from the xorg server, along with the window manager and terminal.
```bash
# Xorg
$ sudo pacman -S xorg-server xorg-xinit mesa
$ sudo pacman -S xorg-twm xorg-xclock alacritty # Basic components

# intel graphics
$ sudo pacman -S xf86-video-intel libva-intel-driver

# Laptop touchpad
$ sudo pacman -S xf86-input-synaptics


# Awesome
$ sudo pacman -S awesome
$ sudo cp /etc/X11/xinit/xinitrc ~/.xinitrc  # basic X initrc configuration
```
Edit the xinitrc file
```bash
$ sudo vim ~/.xinitrc
```

Then, comment the last lines
```bash
twm
xclock -geometry 50x50-1+1 &
xterm -geometry 80x50+494+51 &
xterm -geometry 80x20+494-0 &
exec xterm -geometry 80+66+0+0 -name login
```
And add in the end
```bash
exec awesome
```

After installation of Xorg, we may need to define the keyboard layout for the X server (I had issues, and this was what solved)

Create the keyboard conf file for X
```bash
$ sudo vim /etc/X11/xorg.conf.d/00-keyboard.conf
```
And add this to the file
```vim
Section "InputClass"
       Identifier "system-keyboard"
	   MatchIsKeyboard "on"
	   Option "XbkLayout" "br"
EndSection
```

Save the file and proceed
```bash
$ mkdir -p ~/.config/awesome/
$ cp /etc/xdg/awesome/rc.lua ~/.config/awesome # basic Awesome configuration
```
To have Awesome working with the tools we want, we need to change some few things on the configuration file
```bash
$ vim ~/.config/awesome
```

Locate those lines and change them
```bash
terminal = "alacritty"
editor = os.getenv["EDITOR"] or "vim" # I change to vim, because is the one I installed, remember to set yours.
```

We need a way to login into our system now
```bash
$ sudo pacman -S lightdm
$ sudo pacman -S lightdm-webkit2-greeter
```
Set the default greeter
```bash
$ sudo vim /etc/lightdm/lightdm.conf
```
Change the value on `[Seat:*]`
```
greeter-session=lightdm-webkit2-greeter
```

Add lightdm to start on boot
```bash
$ systemctl enable lightdm.service
```

Install fonts
```bash
$ sudo pacman -S noto-fonts noto-fonts-emoji noto-fonts-extra

$ sudo fc-cache
```

Install Nitrogen for the wallpaper setter
```bash
$ sudo pacman -S nitrogen
```

Install Rofi as app launcher
```bash
$ sudo pacman -S rofi
```

Let's add rofi as the launcher with `Modkey+r`, open the awesome file
```bash
$ vim ~/.config/awesome/rc.lua
```
And change the line
```lua
awful.key({ modkey }, "r", function()...)
```
Change the function in this to:
```lua
function() awful.spawn("rofi -show run") end,
```


At last, we need snap for some packages
```bash
$ yay -S snapd

# Enable on startup
$ sudo systemctl enable --now snapd.socket

# Enable classic snap support
$ sudo ln -s /var/lib/snapd/snap /snap
```

When entering the awesome desktop for the first time, ensure you have the proper keyboard layout.
```bash
$ setxlbmap -layout "br"
```

To set the wallpaper, let's first create the theme directory.

```bash
$ mkdir ~/.config/awesome/themes
$ cp -r /usr/share/awesome/themes/default ~/.config/awesome/themes

# Here you can rename the theme folder
$ mv ~/.config/awesome/themes/default ~/.config/awesome/themes/toro
```
> Obs.: You can change the theme name, I set it for `toro`, but you can set to a different name.

Now change the rc.lua
```lua
-- Coment this line
-- beautiful.init(gears.filesystem.get_configuration_dir() .. "/themes/default/theme.lua")

-- And add this line
local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), "toro")
beautiful.init(theme_path)
```
> Obs.: Remember, if you used a different name for the theme, put the same here, I used `toro` for my

Now, let's make the first change, and add a wallpaper. 
Download the image
```bash
$ wget -O ~/.config/awesome/themes/toro/toro_wallpaper.jpg https://cdna.artstation.com/p/assets/images/images/032/856/752/4k/surendra-rajawat-torriigate-web.jpg?1607671140
```

On the theme.lua file, change the `theme.wallpaper` attribute
```lua
theme.wallpaper = themes_path.."toro/toro_wallpaper.jpg"
```

Now, reload Awesome by pressing `ctrl+super+r`

Skipping the grub menu, to do this we need to change the Grub configuration
```bash
$ sudo vim /etc/default/grub
```
And change (I commented it, in case I need to return to 5seconds later for a reason.)
```vim
#GRUB_TIMEOUT=5
GRUB_TIMEOUT=0
```

And now, let's add an animation for boot process, to look cooler. We are going to use `plymouth`
```bash
$ yay -S plymouth
```

Add plymouth to HOOK for GRUB to load it
```bash
$ sudo vim /etc/mkinitcpio.conf
```
Locate the HOOKS line and add plymouth on the end.
```vim
HOOKS=(base udev ... plymouth)
```

And now prepare the mkinitcpio to run
```bash
$ sudo mkinitcpio -p linux-lts
```
> Obs.: We used linux-lts kernel, so it has to be the same

Now, update the GRUB configuration file
```bash
$ sudo vim /etc/default/grub
```
And change the line
```vim
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3"
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet splash"
```
> I commented it, in case I need to return later for a reason.

Generate the new GRUB configuration
```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

Now, let's configure the theme for plymouth
```bash
# list the installed themes
plymouth-set-default-theme -l
 
 # set the theme
 sudo plymouth-set-default-theme -R <theme_name>
 ```
 > Obs.: You can install or even create different themes!

 And we need to change the display-manager service on boot to match plymouth, as we are using lightDM
 ```bash
 $ sudo systemctl disable lightdm
 $ sudo systemctl enable lightdm-plymouth
 ```

 And reboot to see the magic!

## Tools to install
**Basic:**
| tool               | usage       | to install |
| ------------------ | ----------- | ---------- |
|python - pip - venv | python tols | pacman     |

Utilities: 
| tool             | usage               | to install |
| ---              | ---                 | ---        |
| firefox          | browser             | pacman     |
| mousedpad        | text editor         | pacman     |
| nomacs           | image viewer        | pacman     |
| pcmanfm          | file manager        | pacman     |
| qalculate-qt     | calculator          | pacman     |
| telegram         | chat                | snap       |
| authy            | 2fa                 | snap       |
| bitwarden        | password manager    | snap       |
|onlyoffice-desktop| office suit         | snap       |
| youtube-music    | music app           | snap       |
| iriun            | for android webcam* | yay        |
| android-tools    | for android bridge  | pacman     |
| screenshot tool  | need to find one*   |            |
| obsidian         | Note taking app     |AppImage    |
| snapdrop         | Quick file share    |nativefier* |
| vlc              | Video viewer        |pacman      |

Dev
| tool                    | to install |
| ---                     | ---        |
| codium                  | snap       |
| docker + docker-compose | pacman     |
| poetry                  | on doc     |
| rdm                     | snap       |
| mongo-compose           | snap       |
| git                     | pacman     |
| insomnia                | snap       |
| beekeper-studio         | snap       |



## Environment Setup


# References
- [Color Palette Used](https://coolors.co/041735-f76846-513655-6981a7)
- [Arch Wiki](https://wiki.archlinux.org/title/Main_page)
- [Arch Linux Installation](https://itsfoss.com/install-arch-linux/)
- [After Installation](https://itsfoss.com/things-to-do-after-installing-arch-linux/)
- [Awesome Installation](https://medium.com/@lhennessy/psychedelic-awesome-wm-on-arch-linux-6f8dc053a133)
- [Yay Installation](https://www.makeuseof.com/install-and-use-yay-arch-linux/)
- [Firewall installation](https://www.linuxcapable.com/how-to-install-configure-ufw-firewall-on-arch-linux/)
- [LightDM installation](https://linoxide.com/install-lightdm-arch-linux/)
- [Rofi Configuration](https://linuxconfig.org/how-to-use-and-install-rofi-on-linux-tutorial)
- [Plymouth Configuration](https://www.debugpoint.com/install-plymouth-arch-linux/)
- [Wallpaper by Surendra Rajawat](https://www.artstation.com/artwork/aYdQY2)