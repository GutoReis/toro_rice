# Toro RICE
Toro is my personal Arch Linux RICE customization, this repository contains my dotfiles used on it, and also a shell script to auto install and configure all my system.

Toro is inspired in the Japanese Lanterns which were placed along the way in buddhist temples in Japan to represent the path to enlightenment. The path to enlightenment is about patience, and caring, many arts can be a path to enlightenment, ricing is one of my arts to this path. 🧘
